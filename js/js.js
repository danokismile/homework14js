let btn = document.createElement('button');
btn.textContent = 'New style';
document.body.before(btn);



window.onload = function(){
    if (localStorage.getItem('bodyColor') !== null){
        let color = localStorage.getItem('bodyColor');
        document.getElementsByTagName('body')[0].style.background = color;
    }
    if (localStorage.getItem('mainColor') !== null){
        let mainColor = localStorage.getItem('mainColor');
        document.querySelector('.box').style.color = mainColor
    }

    if(localStorage.getItem("pColor") !== null){
        let pColor = localStorage.getItem("pColor");
        let paragraph = document.getElementsByTagName("p")
        for (let solid of paragraph){
            solid.style.color = pColor
        }
    }

    if(localStorage.getItem('href') !== null){
        let linkColor = localStorage.getItem('href');
        let listColor = document.getElementsByTagName("a");
        for (let item of listColor){
            item.style.color = linkColor
        }
    }

    if(localStorage.getItem('liStyle') !== null){
        let liStyle = localStorage.getItem('liStyle');
        let liList = document.getElementsByTagName("li");
        for (let items of liList){
            items.style.background = liStyle
        }
    }
}
btn.onclick = function(){
    if (localStorage.getItem('bodyColor') !== null){
        function colorAll (){
            localStorage.clear()
            document.location.reload()
        }
        colorAll()
    }
    else{
        document.getElementsByTagName('body')[0].style.background = 'blue'
        localStorage.setItem('bodyColor', 'blue');

        document.querySelector('.box').style.color = 'red'
        localStorage.setItem('mainColor', 'red')

        let par = document.getElementsByTagName("p")
        for (let paragraph of par){
            paragraph.style.color = 'red'
            localStorage.setItem('pColor', 'red');
        }


        let hrefA = document.getElementsByTagName("a")
        for (let href of hrefA){
            href.style.color = 'yellow'
            localStorage.setItem('href', 'yellow');
        }

        let li = document.getElementsByTagName("li")
        for (let lip of li){
            lip.style.background = 'turquoise'
            localStorage.setItem('liStyle', 'turquoise')
        }

    }
}

